        #954315 +(451)- [X]

        <@LAMMJohnson> I suspect Star Trek Into Darkness will be good
        but not great.
        <@LAMMJohnson> My reasoning for this is thus                 
          
        <@LAMMJohnson> Perfect score because all of its words have a
        length that is a power of 2.
        <@LAMMJohnson> Minus one point because total letters is not a
        power of two.
        <@LAMMJohnson> Not even including spaces.
        <@LAMMJohnson> However, the letters in all of the words plus
        the number of words is 24.
        <@LAMMJohnson> Overall score for the movie: 8.5               
                    
        <~chown> LAMMJohnson - Vice Executive of the Autism
        Department      
        <@LAMMJohnson> The score, correctly, is a power of two marred
        with an ugly fraction.
        <@LAMMJohnson> Although the fraction is 1/2
        <@LAMMJohnson> Actually, the overall fraction is 17/20 which I
        pretty much hate.
        <@LAMMJohnson> Which means I will initially think the movie is
        OK but then later change my mind and hate it.
        <~chown> LAMMJohnson - Honourable Chairman of the Autism
        Empire

